/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.48090419039869814, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.14825581395348839, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.09523809523809523, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.7073248407643312, 500, 1500, "getUpcomingEvents"], "isController": false}, {"data": [0.12603305785123967, 500, 1500, "getListDomainByLevel"], "isController": false}, {"data": [0.10743801652892562, 500, 1500, "getPendingEvents"], "isController": false}, {"data": [0.6007978723404256, 500, 1500, "getPortfolioTermByYear"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.7087347463070006, 500, 1500, "findAllClassResources"], "isController": false}, {"data": [0.11764705882352941, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.10256410256410256, 500, 1500, "getAllEvents"], "isController": false}, {"data": [0.15079365079365079, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.02142857142857143, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.11397058823529412, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.11073825503355705, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.36106750392464676, 500, 1500, "getMyDownloadPortfolio"], "isController": false}, {"data": [0.62914093206064, 500, 1500, "getListDomain"], "isController": false}, {"data": [0.0967741935483871, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.7115743621655258, 500, 1500, "getLessonPlan"], "isController": false}, {"data": [0.1206896551724138, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.06687898089171974, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.15126050420168066, 500, 1500, "getChildSemesterEvaluation"], "isController": false}, {"data": [0.2058139534883721, 500, 1500, "getCentreManagementConfig"], "isController": false}, {"data": [0.5065005417118094, 500, 1500, "updateClassResourceOrder"], "isController": false}, {"data": [0.0, 500, 1500, "getChildPortfolio"], "isController": false}, {"data": [0.7059948979591837, 500, 1500, "getStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCentreForSchool"], "isController": false}, {"data": [0.12202380952380952, 500, 1500, "portfolioByID"], "isController": false}, {"data": [0.104, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.12403100775193798, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.7190563725490197, 500, 1500, "getCountStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.164804469273743, 500, 1500, "getPastEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.3549920760697306, 500, 1500, "getMyDownloadAlbum"], "isController": false}, {"data": [0.12581699346405228, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.008620689655172414, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.10893854748603352, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.030927835051546393, 500, 1500, "getChildChecklist"], "isController": false}, {"data": [0.12096774193548387, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 19664, 0, 0.0, 3117.9898291293757, 18, 66127, 948.0, 9227.5, 13617.0, 21911.35, 62.55149269161643, 633.5762171361315, 94.32456164112575], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllFeeGroup", 344, 0, 0.0, 4024.450581395347, 117, 19169, 2891.5, 8662.5, 11019.0, 16220.500000000011, 1.1397974871441445, 1.0462984745268515, 1.2277310823437417], "isController": false}, {"data": ["findAllChildActiveSubsidies", 252, 0, 0.0, 5530.416666666668, 210, 16211, 4892.5, 10642.6, 12656.35, 14941.85, 0.829040090010067, 1.2362736498489961, 1.351238193580861], "isController": false}, {"data": ["getUpcomingEvents", 1570, 0, 0.0, 880.7789808917197, 30, 10787, 385.0, 2277.900000000001, 3478.3499999999995, 7332.359999999997, 5.217819018119458, 3.042029251774723, 7.490423785776956], "isController": false}, {"data": ["getListDomainByLevel", 242, 0, 0.0, 5775.466942148761, 149, 33887, 4829.0, 11877.2, 14141.149999999998, 22456.459999999963, 0.79429160348438, 4.1839546121428155, 1.412504892524469], "isController": false}, {"data": ["getPendingEvents", 242, 0, 0.0, 5735.640495867765, 153, 33645, 5177.0, 11382.2, 13345.299999999997, 16580.489999999998, 0.7956338769068911, 0.3970399522455286, 1.0349456289452919], "isController": false}, {"data": ["getPortfolioTermByYear", 1880, 0, 0.0, 1471.8723404255315, 18, 18832, 549.0, 4113.300000000001, 5858.799999999996, 11049.270000000019, 6.209805547206083, 5.018035893006371, 7.897319043417441], "isController": false}, {"data": ["findAllCurrentFeeTier", 82, 0, 0.0, 17069.68292682927, 1715, 30245, 18990.0, 22867.9, 23836.099999999995, 30245.0, 0.26955772808857276, 1.989317285471496, 0.6823179992241997], "isController": false}, {"data": ["findAllClassResources", 1557, 0, 0.0, 888.3808606294167, 34, 12725, 387.0, 2231.0000000000005, 3334.199999999998, 7088.4400000000205, 5.15417845970505, 3.0250598186354836, 7.157462665723224], "isController": false}, {"data": ["listBulkInvoiceRequest", 153, 0, 0.0, 9168.437908496731, 156, 29431, 8555.0, 17004.6, 19076.2, 24565.06000000007, 0.5017331107780798, 0.7499960802100721, 0.7599492722820331], "isController": false}, {"data": ["getAllEvents", 156, 0, 0.0, 8956.416666666662, 194, 23104, 9054.5, 16065.600000000002, 17826.100000000002, 22316.83000000001, 0.5116683339619856, 3.288868138806435, 0.913407924299326], "isController": false}, {"data": ["findAllCustomSubsidies", 315, 0, 0.0, 4403.809523809525, 160, 16584, 3599.0, 9748.800000000008, 12286.599999999997, 15997.079999999974, 1.0381888712744676, 1.2401908918289593, 1.2977360890930842], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 140, 0, 0.0, 9951.992857142854, 708, 21911, 9482.0, 18143.100000000002, 19577.3, 21790.05, 0.45815266301235374, 38.18930947291991, 0.6599366972101776], "isController": false}, {"data": ["getAllChildDiscounts", 272, 0, 0.0, 5096.5845588235325, 147, 22384, 4316.0, 10835.900000000001, 12846.299999999981, 15827.95999999998, 0.8987100866990908, 0.5494067522203425, 1.4805897619739903], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 149, 0, 0.0, 9359.060402684565, 155, 22919, 8985.0, 17293.0, 19742.5, 22849.0, 0.4887040486998504, 0.351733285050576, 0.6409468138709953], "isController": false}, {"data": ["getMyDownloadPortfolio", 637, 0, 0.0, 2175.7315541601247, 107, 14673, 1427.0, 4898.400000000002, 7002.100000000002, 10677.960000000001, 2.105569346612413, 1.2378444791608132, 2.1610872883687953], "isController": false}, {"data": ["getListDomain", 1781, 0, 0.0, 1561.25435148793, 28, 23683, 500.0, 4499.199999999999, 7441.7999999999865, 13501.740000000016, 5.851969166266898, 8.642774822403744, 9.517792688817186], "isController": false}, {"data": ["getAdvancePaymentReceipts", 248, 0, 0.0, 5615.758064516127, 138, 19211, 5157.5, 10851.999999999998, 13438.449999999997, 18601.64, 0.8139260114999869, 1.224068415732402, 1.2979894304487094], "isController": false}, {"data": ["getLessonPlan", 1607, 0, 0.0, 861.1356565028011, 31, 12363, 382.0, 2124.6000000000004, 3216.3999999999987, 6615.6400000000285, 5.309184856764337, 3.458228808068177, 8.513360873835003], "isController": false}, {"data": ["findAllProgramBillingUpload", 174, 0, 0.0, 8012.8218390804595, 132, 21857, 7207.5, 15687.5, 17767.0, 21835.25, 0.5704730992426478, 0.7729542935067703, 0.8506957251401593], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 157, 0, 0.0, 8867.47133757962, 204, 27160, 7844.0, 15867.800000000007, 19904.2, 24566.239999999943, 0.5180082089453749, 0.7429580939195074, 0.9313018678402687], "isController": false}, {"data": ["getChildSemesterEvaluation", 357, 0, 0.0, 3897.764705882353, 110, 20772, 3173.0, 8223.4, 9850.099999999995, 15688.480000000007, 1.1777630419936855, 0.9074756251299003, 1.8057499764942249], "isController": false}, {"data": ["getCentreManagementConfig", 430, 0, 0.0, 3228.467441860467, 150, 14212, 2451.0, 6977.000000000003, 9592.049999999997, 12664.13, 1.4178083320968726, 1.6725707667705292, 2.2956310689615376], "isController": false}, {"data": ["updateClassResourceOrder", 923, 0, 0.0, 1499.959913326109, 69, 16100, 830.0, 3657.0, 5157.0, 8123.279999999999, 3.0469990525582578, 1.79725334740741, 2.8982198019450616], "isController": false}, {"data": ["getChildPortfolio", 51, 0, 0.0, 27663.529411764706, 5295, 43955, 28623.0, 40373.600000000006, 41521.2, 43955.0, 0.16543724089608594, 0.31097411515405127, 0.4001836383785204], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 1568, 0, 0.0, 882.3698979591846, 29, 15972, 392.0, 2205.500000000001, 3605.0499999999993, 6342.4199999999955, 5.203528310778073, 3.1150027876044524, 9.497455481293184], "isController": false}, {"data": ["findAllCentreForSchool", 149, 0, 0.0, 19020.389261744975, 5682, 32488, 19451.0, 25012.0, 28928.0, 32374.5, 0.48268489423045774, 439.4874307557744, 1.1868924679694839], "isController": false}, {"data": ["portfolioByID", 168, 0, 0.0, 8272.30357142857, 227, 23095, 7569.0, 15862.399999999998, 16844.6, 22327.030000000002, 0.5555996507659338, 0.8850767804405774, 1.9527374444400352], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 125, 0, 0.0, 11146.064000000002, 140, 31717, 10607.0, 19634.200000000004, 21795.399999999994, 31615.079999999998, 0.411346584177965, 0.3310054544557062, 0.5475247990571936], "isController": false}, {"data": ["invoicesByFkChild", 129, 0, 0.0, 10834.449612403107, 150, 26867, 11230.0, 18910.0, 21112.0, 26593.69999999999, 0.42348538318861517, 1.192209966884428, 1.2307543948919126], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 1632, 0, 0.0, 847.7634803921567, 40, 17648, 372.0, 2207.6000000000004, 3201.7, 5615.080000000002, 5.40515476892301, 3.2620953585883004, 5.906609557055515], "isController": false}, {"data": ["findAllFeeDraft", 179, 0, 0.0, 7835.184357541901, 1926, 17551, 7547.0, 12429.0, 13238.0, 16210.999999999982, 0.5848106063081134, 0.3323825906946505, 0.9314707996958331], "isController": false}, {"data": ["getPastEvents", 358, 0, 0.0, 3882.4916201117294, 125, 20938, 3193.5, 8642.00000000001, 10549.350000000002, 14944.21000000002, 1.183564977056031, 0.5871591878363903, 1.4910144730491015], "isController": false}, {"data": ["findAllConsolidatedRefund", 123, 0, 0.0, 11321.699186991873, 4663, 21739, 10809.0, 17096.2, 18276.6, 21652.120000000003, 0.4041081038459264, 0.5404021861262788, 0.6981125348666443], "isController": false}, {"data": ["getMyDownloadAlbum", 631, 0, 0.0, 2198.687797147383, 70, 15518, 1467.0, 4885.6, 7106.799999999997, 10229.199999999997, 2.0805307166785365, 1.3612847462642768, 3.285369305536322], "isController": false}, {"data": ["getRefundChildBalance", 306, 0, 0.0, 4535.758169934641, 148, 20999, 3748.0, 9474.600000000002, 12289.74999999999, 17336.020000000004, 1.0088555094720322, 0.9625506179239994, 1.5792923649254567], "isController": false}, {"data": ["findAllUploadedGiroFiles", 116, 0, 0.0, 12114.13793103448, 1386, 26753, 11848.5, 19167.399999999998, 20663.649999999994, 26604.25, 0.3771879338880597, 107.2943918037631, 0.539629221822273], "isController": false}, {"data": ["findAllInvoice", 179, 0, 0.0, 15690.97206703911, 206, 66127, 11348.0, 47692.0, 54903.0, 64485.39999999998, 0.5699638598334686, 0.9209797049879798, 1.8218905227587525], "isController": false}, {"data": ["getChildChecklist", 97, 0, 0.0, 14354.453608247422, 628, 27625, 13847.0, 23386.2, 25060.399999999998, 27625.0, 0.31821641340576856, 0.7526051405893236, 1.047876705082277], "isController": false}, {"data": ["bankAccountInfoByIDChild", 124, 0, 0.0, 11240.435483870968, 133, 29101, 10811.0, 20133.0, 22538.5, 28762.75, 0.40711398863364007, 0.5873424690478457, 0.9370778039155172], "isController": false}, {"data": ["findAllCreditDebitNotes", 61, 0, 0.0, 23074.032786885244, 5362, 39625, 23098.0, 33248.4, 36885.0, 39625.0, 0.199199934688546, 0.36185989672626334, 0.46531859743652537], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 19664, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
